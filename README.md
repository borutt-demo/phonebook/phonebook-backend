# Phone book back-end project

This project is a back-end service part of a phone book client-service projects pair. The project consists of two Java EE projects, a back-end service API and its implementation.

The back end provides a RESTful API and its implementation of the phone book's `contacts` resource. Its persistence layer consists of a JPA-based relational database.

> This project is intended solely for testing and demonstration purposes.

> [Related Phone book UI project](https://gitlab.com/borutt-demo/phonebook/phonebook-ui)

## Getting Started

The following instructions let you build the project's Java EE war artifact, provide you with clues to prepare the deployment environment and run the application.

This project is a compile-time and run-time dependency of the corresponding [phone book UI project](https://gitlab.com/borutt-demo/phonebook/phonebook-ui). 

### Requirements
* [Maven](https://maven.apache.org/) 3.6.0 or newer
* JDK 1.8.x
* [Wildfly](http://wildfly.org/downloads/) 8 or newer or other fully Java EE 7 compatible application server (tested with Wildfly 13.0.0)
* MySQL server 5.5 or newer
* [MySQL JDBC connector](https://dev.mysql.com/downloads/connector/j/) 8.0.x or newer

## Building and Installing

### Maven

Build WAR artifact from source code and install it into a local Maven repository:

    $ ./mvn clean install

> This artifact must be installed in a Maven repository prior to building the code of the UI project.

> The above command also creates a Javadoc JAR artifact and a source code JAR artifact for each subproject `phonebook-api` and `phonebook-impl`. All created artifacts become available in a local Maven repository and in subprojects' subdirectory `./target`.

## Deploying to application server

The built war artifact may be deployed to any application server that fully supports Java EE 7 specification. These instructions pertain to Wildfly configuration and deployment. These instructions assume that both, the UI and back-end artifact, share the same application server instance.

### Database initialization

The project code currently provides only MySQL DB schema definition.

#### MySQL initialization

* Create a database, a user, set grants to allow modifications on the database tables.
* In MySQL environment execute DDL/DML SQL statements from sequence of files located in the project's subdirectory `config/db/mysql/`.

### Wildfly configuration

Follow the guide [Installing the MySQL Datasource on WildFly in Standalone mode](http://www.mastertheboss.com/jboss-server/jboss-datasource/configuring-a-datasource-with-mysql-on-wildfly) to install a MySQL JDBC driver to Wildfly and configure a Wildfly data source. Make following changes to the instructions form the guide:
* acquire the latest 8.x MySQL JDBC connector if MySQL server is version 5.5 or newer;
* change Wildfly command

```
data-source add --jndi-name=java:/MySqlDS --name=MySQLPool --connection -url=jdbc:mysql://172.17.0.2:3306/mysqlschema --driver-name=mysql --user-name=jboss --password=jboss
```

to

```
data-source add --jndi-name=java:/PhonebookDb --name=PhonebookDb --connection-url=jdbc:mysql://localhost:3306/dbname?useUnicode=yes&characterEncoding=UTF-8 --driver-name=mysql --user-name=dbuser --password=dbpass
```

and replace `dbname`, `dbuser`, `dbpass` to reflect your database system configuration. Optionally replace DB server host and port.

> JDBC URL parameters `useUnicode=yes` and `characterEncoding=UTF-8` are required for general unicode support.

### Wildfly deployment

Start Wildfly instance. Copy built UI and back-end war artifacts to Wildfly deployment directory.

> Default Wildfly deployment directory is `<wildfly_isntallation_directory>/standalone/deployments`    

> To configure Wildfly deployment scanner and manage its marker files refer to the [Deploying Using the Deployment Scanner documentation](https://docs.jboss.org/author/display/WFLY/Application+deployment#Applicationdeployment-filesystemdeployments).

## Authors
* Borut Turšič
