CREATE TABLE `Kontakt` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `name` varchar(100) NOT NULL,
    `surname` varchar(100) NOT NULL,
    `address` varchar(255) DEFAULT NULL,
    `phone` varchar(100) DEFAULT NULL,
    `mobile` varchar(100) DEFAULT NULL,
    `email` varchar(100) DEFAULT NULL,
    `note` text,
    `changed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;