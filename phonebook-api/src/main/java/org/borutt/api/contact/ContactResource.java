package org.borutt.api.contact;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Personal contact resource operations API with basic CRUD operations.
 * 
 * @see Contact
 */
@Path("/contacts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ContactResource {

    @POST
    public void create(Contact contact);

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id);

    @PUT
    public void update(Contact contact);

    @GET
    @Path("{id}")
    public Contact get(@PathParam("id") Long id);

    @GET
    public List<Contact> findAll();
}