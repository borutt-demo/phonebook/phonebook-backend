package org.borutt.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Custom email validator tag is intended for bean validation framework up to,
 * including, version 1.1 (JSR 349, supported by Java EE 7). Bean validation 2.0
 * (JSR 380, supported by Java EE 8) contains email validator and thus renders
 * this validator tag obsolete.
 */
@Constraint(validatedBy = { EmailValidator.class })
@Target({ PARAMETER, FIELD })
@Retention(RUNTIME)
@Documented
public @interface Email {
    String message() default "Invalid email address";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
