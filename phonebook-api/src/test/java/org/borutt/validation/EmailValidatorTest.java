package org.borutt.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class EmailValidatorTest {

    private EmailValidator validator;

    @BeforeEach
    public void setUp() {
        validator = new EmailValidator();
    }

    @Test
    public void givenValidEmail_whenValidate_ThenValid() {
        // GIVEN
        String email = "abc@def.com";
        // WHEN
        boolean valid = validator.isValid(email, null);
        // THEN
        assertTrue(valid);
    }

    @Test
    public void givenNull_whenValidate_ThenValid() {
        // GIVEN
        String email = null;
        // WHEN
        boolean valid = validator.isValid(email, null);
        // THEN
        assertTrue(valid);
    }

    @Test
    public void givenEmptyString_whenValidate_ThenValid() {
        // GIVEN
        String email = "";
        // WHEN
        boolean valid = validator.isValid(email, null);
        // THEN
        assertTrue(valid);
    }

    @ParameterizedTest(name = "run #{index} with [{arguments}]")
    @ValueSource(strings = { "abc", "abc@cde", "cde.com", "@cde.com" })
    public void givenInvalidEmail_whenValidate_ThenValid(String email) {
        // WHEN
        boolean valid = validator.isValid(email, null);
        // THEN
        assertFalse(valid);
    }

}
