package org.borutt.impl.contact;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.borutt.api.contact.Contact;
import org.borutt.api.contact.ContactResource;
import org.borutt.db.ContactDao;

@Stateless
public class ContactResourceImpl implements ContactResource {

    @Inject
    private ContactDao contactDao;

    @Override
    public void create(Contact contact) {
        contactDao.create(contact);
    }

    @Override
    public void delete(Long id) {
        contactDao.delete(id);
    }

    @Override
    public void update(Contact contact) {
        contactDao.update(contact);
    }

    @Override
    public Contact get(Long id) {
        return contactDao.find(id);
    }

    @Override
    public List<Contact> findAll() {
        return contactDao.findAll();
    }

}