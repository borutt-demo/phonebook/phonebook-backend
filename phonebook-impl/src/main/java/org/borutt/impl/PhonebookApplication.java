package org.borutt.impl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS application context of the Phonebook application.
 *
 */
@ApplicationPath("/phonebook")
public class PhonebookApplication extends Application {
}
