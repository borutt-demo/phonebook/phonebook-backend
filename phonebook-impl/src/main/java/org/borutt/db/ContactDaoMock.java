package org.borutt.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.enterprise.inject.Alternative;

import org.borutt.api.contact.Contact;

@Alternative
public class ContactDaoMock implements Dao<Long, Contact> {

    private final Map<Long, Contact> contacts = new ConcurrentHashMap<>();
    private final AtomicLong keyGen = new AtomicLong(0);

    @Override
    public Contact create(Contact contact) {
        if (contact.getId() != null && contact.getId() >= 0) {
            throw new IllegalArgumentException("Contact to be created already contains an ID");
        }
        contact.setId(keyGen.incrementAndGet());
        Contact existingContact = contacts.put(contact.getId(), contact);
        if (existingContact != null) {
            throw new IllegalStateException("Created contact ID is not unique. ID=" + contact.getId());
        }
        return contact;
    }

    @Override
    public Contact update(Contact contact) {
        Contact existingContact = contacts.put(contact.getId(), contact);
        if (existingContact == null) {
            throw new IllegalStateException(
                    String.format("Updating contact with ID=%d does not exist in storage.", contact.getId()));
        }
        return contact;
    }

    @Override
    public void delete(Long id) {
        Contact existingContact = contacts.remove(id);
        if (existingContact == null) {
            throw new IllegalStateException(
                    String.format("Deleting contact with ID=%d does not exist in storage.", id));
        }
    }

    @Override
    public Contact find(Long id) {
        Contact existingContact = contacts.get(id);
        if (existingContact == null) {
            throw new IllegalStateException(String.format("COntact with ID=%d cannot be found in storage.", id));
        }
        return existingContact;
    }

    @Override
    public List<Contact> findAll() {
        return new ArrayList<>(contacts.values());
    }

    @Override
    public List<Contact> findRange(int minIdx, int maxIdx) {
        return contacts.values().stream().filter(contact -> contact.getId() >= minIdx && contact.getId() <= maxIdx)
                .collect(Collectors.toCollection(ArrayList::new));
    }

}
