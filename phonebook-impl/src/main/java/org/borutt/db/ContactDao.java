package org.borutt.db;

import org.borutt.api.contact.Contact;

public interface ContactDao extends Dao<Long, Contact> {

}
