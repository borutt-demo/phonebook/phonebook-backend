package org.borutt.db.jpa;

import javax.ejb.Stateless;

import org.borutt.api.contact.Contact;
import org.borutt.db.ContactDao;

@Stateless
public class ContactJpaDao extends JpaDao<Long, Contact> implements ContactDao {

}
