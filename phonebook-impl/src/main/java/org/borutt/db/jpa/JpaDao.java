package org.borutt.db.jpa;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import org.borutt.db.Dao;

public abstract class JpaDao<K, E> implements Dao<K, E> {

    protected Class<E> entityClass;

    @PersistenceContext(unitName = "phonebook")
    protected EntityManager em;

    @SuppressWarnings("unchecked")
    public JpaDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
    }

    @Override
    public E create(E e) {
        em.persist(e);
        // Reads DB generated entity ID to persistence context.
        em.flush();
        return e;
    }

    @Override
    public E update(E e) {
        return em.merge(e);
    }

    @Override
    public void delete(K id) {
        em.remove(em.find(entityClass, id));
    }

    @Override
    public E find(K id) {
        return em.find(entityClass, id);
    }

    @Override
    public List<E> findAll() {
        CriteriaQuery<E> cq = em.getCriteriaBuilder().createQuery(entityClass);
        cq.select(cq.from(entityClass));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<E> findRange(int minIdx, int maxIdx) {
        CriteriaQuery<E> cq = em.getCriteriaBuilder().createQuery(entityClass);
        cq.select(cq.from(entityClass));
        TypedQuery<E> query = em.createQuery(cq);
        query.setMaxResults(maxIdx - minIdx);
        query.setFirstResult(minIdx);
        return query.getResultList();
    }

}
